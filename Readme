Provisioning:
  The system uses Ansible to provision the vagrant machine. The ansible playook
  is provided in playbook.yml file. This playbook installs some required packages
  for the deployment system. Most importantly it uses docker and docker swarm
  for the deployment of applications. Docker was chosen as a deployment system
  because it enables easy deployment of applications on a variety of systems.
  It also allows us to easily update, rollback and scale applications. Docker
  swarm comes with docker by default and this is why it is easy choice for
  container orchestration. It could be replaced with Amazon ECS/EKS or even
  with custom kubernetes cluster. Docker swarm is also initialized in the
  provisioning script. The address of docker swarm is 192.168.33.3

  To provision create vagrant machine run the following command:
  vagrant up

  To manually provision in case of errors run:
  vagrant provision

  To go inside the vagrant virtual machine use:
  vagrant ssh

Deployment:
  To wrap the example java app I have included the Dockerfile in
  java-app/Dockerfile file.

  Build the service:
    In java-app directory from terminal execute the following command to
    build the container:

    docker build -f Dockerfile -t java-app .

  Deploy the service:
    To create the service using docker swarm execute the following command:
    docker service create --publish 8080:8080 --limit-memory 500m --name java-app java-app

    The --limit-memory argument specifies how much memory you want to give to
    the container. In the example 500 MB is set. It can also be set dynamically
    using this command or by setting the specific values of JVM
    (e.g -XX:MaxRAMFraction):

    grep MemTotal /proc/meminfo | awk '{print $2 / 1024 * .8}'
    The above command will give the 80% of available memory and this can be
    passed to the container.

  Update the service:
  To update the service either build the service with a new tag or if the
  same tag is to be used then you can restart the service with this command:
  docker service update --force --update-parallelism 1 --update-delay 30s java-app

  Additionally there are many options which can be set in update command.
  One interesting option is to set --replicas to set the number of tasks to run.
  This can be used to loadbalance or handling failovers of the application.

  Rollback the service:
  A service configuration can be rollbacked with:
  docker service rollback java-app

  Stop the service:
  To completely stop the service use the following command:
  docker service rm java-app

  Application:
  The version of JVM was selected in Dockerfile which can be changed when a
  new version is required. Additionally more dependencies can be installed
  inside the Dockerfile. This makes is easy to create reproduceable builds and
  easy to deploy applications without installing any additional tools and
  dependencies on the host machine.

  Performance:
  Performance can be tuned by setting up different JVM parameters such as
  Garbage Collection, Min and Max Heap size, Permanent Generation (PermGen) and
  much more. However, these performance tunings are application specific and
  should be used when you have in depth knowledge of the application. In this
  case I will let the default parameters do their job as they are good in most
  scenarios.



Analysis of the application:
  I analyzed the java application by using curl and apache benchmarks. It
  seemes that the delay was introduced manually in the application. I base this
  conclusion by making multiple requests to the endpoint. The commands used to
  do this analysis were:
  curl -i http://192.168.33.3:8080/
  and
  ab -n 20 -c 2 http://192.168.33.3:8080/

  Sources:
    There can be number of sources of the latency in web application. Most
    commonly the delays can occur when resources are busy. For example, a web
    server can slow down when there are a large number of requests coming and
    server cannot handle them with limited CPU or memory. Network bandwidth and
    database can be some other sources of latency.

    However, in the application provided these sources do not apply. The reason
    is the CPU and memory for such small number of requests is good enough.
    Additionally there is no database so there is no latency there. One possible
    source can be synchronization of resources in the application. Application
    can lock objects in order to allow safe access from different threads. In
    this case the application can lock the object which is used to count the
    number of requests. Even in this case such small number of requests should
    not cause delay in few seconds.

    Programming bugs can be also a source of latency. It is possible the bug
    was introduced by not using proper algorithms or by not using proper
    synchronization and programming constructs. For example, an application
    can use Reader/Writer lock appropriately. If a thread wants to only read
    then it only needs to acqiore a read lock instead of write lock which might
    slow the application down significantly. On the other hand if a thread
    wants to write the object then it needs to acquire a write lock which will
    also lock all reads.
